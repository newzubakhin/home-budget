package com.dsr.homebudget.data.repository

import com.dsr.homebudget.data.entity.Category
import org.springframework.data.repository.CrudRepository

interface CategoryRepository : CrudRepository<Category, Long>