package com.dsr.homebudget.data.repository

import com.dsr.homebudget.data.entity.Account
import org.springframework.data.repository.CrudRepository

interface AccountRepository : CrudRepository<Account, Long>