package com.dsr.homebudget.data.repository

import com.dsr.homebudget.data.entity.Currency
import org.springframework.data.repository.CrudRepository

interface CurrencyRepository : CrudRepository<Currency, Long>