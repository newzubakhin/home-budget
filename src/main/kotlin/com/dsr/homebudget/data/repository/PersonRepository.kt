package com.dsr.homebudget.data.repository

import com.dsr.homebudget.data.entity.Person
import org.springframework.data.repository.CrudRepository

interface PersonRepository : CrudRepository<Person, Long>