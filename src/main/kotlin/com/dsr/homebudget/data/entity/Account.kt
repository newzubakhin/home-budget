package com.dsr.homebudget.data.entity

import org.hibernate.validator.constraints.Length
import java.math.BigDecimal
import java.time.OffsetDateTime
import javax.persistence.*

@Entity
data class Account(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long? = null,

        @Length
        val name: String,

        @Length
        val description: String,

        @OneToOne
        val currency: Currency? = null,

        @ManyToOne
        val owner: Person? = null,

        val createDate: OffsetDateTime,

        val currentValue: BigDecimal
)