package com.dsr.homebudget.data.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id


@Entity
data class Currency(
        @Id
        @Column(length = 3)
        val code: String,

        @Column(length = 40)
        val humanReadableName: String
)