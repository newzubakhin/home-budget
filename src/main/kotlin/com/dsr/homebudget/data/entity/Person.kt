package com.dsr.homebudget.data.entity

import org.hibernate.validator.constraints.Length
import java.time.OffsetDateTime
import javax.persistence.*

@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = ["username"])])
data class Person(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long?,

        @Length(min = 1, max = 40)
        @Column(name = "username", unique = true)
        val username: String,

        @Length(max = 256)
        val password: String? = null,

        val registerDate: OffsetDateTime,

        @Length(max = 256)
        val fullName: String

)