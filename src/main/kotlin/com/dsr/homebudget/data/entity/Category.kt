package com.dsr.homebudget.data.entity

import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = ["owner_id", "name"])])
data class Category(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long? = null,

        @Length(min = 1, max = 256)
        val name: String,

        @Length(max = 1024)
        val description: String,

        @ManyToOne
        val owner: Person? = null
)