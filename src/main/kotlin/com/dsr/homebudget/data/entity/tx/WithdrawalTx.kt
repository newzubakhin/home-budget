package com.dsr.homebudget.data.entity.tx

import com.dsr.homebudget.data.entity.Account
import com.dsr.homebudget.data.entity.Category
import com.dsr.homebudget.data.entity.Currency
import com.dsr.homebudget.data.entity.Tag
import java.math.BigDecimal
import java.time.OffsetDateTime
import javax.persistence.Entity
import javax.persistence.OneToOne


@Entity
class WithdrawalTx(
        id: Long?,
        src: Account?,
        newValue: BigDecimal?,
        reason: String,
        createDate: OffsetDateTime,
        category: Category?,
        tagSet: Set<Tag>?,
        val amount: BigDecimal? = null,
        @OneToOne
        val currency: Currency? = null

) : Transaction(id, src, newValue, reason, createDate, category, tagSet)
