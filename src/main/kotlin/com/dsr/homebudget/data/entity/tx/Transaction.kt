package com.dsr.homebudget.data.entity.tx

import com.dsr.homebudget.data.entity.Account
import com.dsr.homebudget.data.entity.Category
import com.dsr.homebudget.data.entity.Tag
import org.hibernate.validator.constraints.Length
import java.math.BigDecimal
import java.time.OffsetDateTime
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
abstract class Transaction(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long? = null,

        @ManyToOne
        val src: Account? = null,

        val newValue: BigDecimal? = null,

        @Length(max = 1024)
        val reason: String,

        val createDate: OffsetDateTime,

        @OneToOne
        val category: Category? = null,

        @ManyToMany
        @JoinColumn(name = "transaction_id")
        val tagSet: Set<Tag>? = null
)