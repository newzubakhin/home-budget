package com.dsr.homebudget.data.entity

import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = ["name", "owner_id"])])
data class Tag(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        val id: Long?,

        @Length(min = 1, max = 64)
        val name: String,

        @ManyToOne
        val owner: Person?

)
