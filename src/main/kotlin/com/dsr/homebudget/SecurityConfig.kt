package com.dsr.homebudget

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class SecurityConfig(
//        @Autowired val principalDetailService
) : WebSecurityConfigurerAdapter() {
    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    override fun configure(auth: AuthenticationManagerBuilder?) {
        val provider = DaoAuthenticationProvider()
        provider.apply {
            setPasswordEncoder(encoder())
        }
        auth?.authenticationProvider(provider)
    }

    @Bean
    fun encoder(): PasswordEncoder {
        return BCryptPasswordEncoder(7)
    }

    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
                .mvcMatchers("/auth/**").anonymous()
                .mvcMatchers("/**").authenticated()
                .and().csrf().disable()
    }
}