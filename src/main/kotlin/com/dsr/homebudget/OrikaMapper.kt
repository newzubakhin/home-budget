package com.dsr.homebudget

import ma.glasnost.orika.impl.ConfigurableMapper
import org.springframework.stereotype.Component

@Component
class OrikaMapper : ConfigurableMapper()

