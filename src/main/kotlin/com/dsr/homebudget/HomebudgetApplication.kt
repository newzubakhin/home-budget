package com.dsr.homebudget

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HomebudgetApplication

fun main(args: Array<String>) {
    runApplication<HomebudgetApplication>(*args)
}
